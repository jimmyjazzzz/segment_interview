package erogner.apiimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import erogner.api.AccountApiDelegate;
import erogner.model.Account;
import erogner.repository.AccountRepository;

@Service
public class AccountApiImpl implements AccountApiDelegate {

    @Autowired
    AccountRepository repo;

    @Override
    public ResponseEntity<List<Account>> accountGet() {
        return new ResponseEntity<>(repo.findAll(), HttpStatus.OK);
    }

    /**
     * POST /account : Create Account
     *
     * @param body Account (required)
     * @return Invalid input (status code 405)
     * @see AccountApi#addAccount
     */
    @Override
    public ResponseEntity<Void> addAccount(Account body) {
        repo.insert(body);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * DELETE /account/{name} : Deletes an Account
     *
     * @param name Name to delete (required)
     * @return Accounts not found (status code 404)
     * @see AccountApi#deleteAccountByName
     */
    @Override
    public ResponseEntity<Void> deleteAccountByName(String name) {
        repo.deleteAccountsByName(name);
        return new ResponseEntity<>(HttpStatus.OK);

    }

    /**
     * GET /account/{name} : Get Accounts
     *
     * @param name name to get (required)
     * @return successful operation (status code 200)
     *         or Accounts not found (status code 404)
     * @see AccountApi#getAccountsByName
     */
    @Override
    public ResponseEntity<Account> getAccountsByName(String name) {
        return new ResponseEntity<>(repo.getAccountsByName(name), HttpStatus.OK);
    }

}