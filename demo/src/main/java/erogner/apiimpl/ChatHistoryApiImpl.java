package erogner.apiimpl;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import erogner.api.ChatHistoryApiDelegate;
import erogner.model.ChatMessage;
import erogner.repository.ChatHistoryRepository;

@Service
public class ChatHistoryApiImpl implements ChatHistoryApiDelegate {

    @Autowired
    ChatHistoryRepository repo;
    
    @Override
    public ResponseEntity<List<ChatMessage>> getChatHistory(String user1, String user2) {

        List<ChatMessage> chatHistory = repo.getChatMessageByFromUserAndToUser(user1, user2);
        chatHistory.addAll(repo.getChatMessageByFromUserAndToUser(user2, user1));
        Collections.sort(chatHistory, new Comparator<ChatMessage>() {
            
            @Override
            public int compare (ChatMessage m1, ChatMessage m2) {
                return m1.getTimestamp().compareTo(m2.getTimestamp());
            }
        });

        return new ResponseEntity<>(chatHistory, HttpStatus.OK);
    }
}