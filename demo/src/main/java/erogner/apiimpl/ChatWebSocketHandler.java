package erogner.apiimpl;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import erogner.model.ChatMessage;
import erogner.model.WSMessage;
import erogner.repository.ChatHistoryRepository;

@Component
public class ChatWebSocketHandler extends TextWebSocketHandler {

    // Map from UserName to the set of active Sessions that they have to listen to
    // chats.
    Map<String, Set<WebSocketSession>> userSessionMap = new ConcurrentHashMap<>();

    ChatHistoryRepository chatRepo;

    public ChatWebSocketHandler(ChatHistoryRepository chatRepo) {
        this.chatRepo = chatRepo;
	}

	@Override
    protected void handleTextMessage(WebSocketSession session, TextMessage textMessage) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();
        String messageString = textMessage.getPayload();
        WSMessage message = objectMapper.readValue(messageString, WSMessage.class);
        switch (message.getMessageType()) {
            case LOGIN: {
                String username = message.getPayload().getUsername();
                Set<WebSocketSession> sessions = userSessionMap.computeIfAbsent(username, k -> new HashSet<>());
                sessions.add(session);
                break;
            }
            case LOGOUT: {
                break;
            }
            case CHAT: {
                processChat(message, messageString);
                break;
            }
        }

    }

    private void processChat(WSMessage message, String messageString) {
        ChatMessage chatMessage = message.getPayload().getMessage();

        // Send the message to all of the sessions of the to User.
        String toUser = chatMessage.getToUser();
        Set<WebSocketSession> origSessionsToSend = userSessionMap.get(toUser);
        if (origSessionsToSend != null) {
            Set<WebSocketSession> sessionsToSend = new HashSet<>(origSessionsToSend);
            for (WebSocketSession session : sessionsToSend) {
                TextMessage messageToSend = new TextMessage(messageString);
                try {
                    session.sendMessage(messageToSend);
                } catch (IOException e) {
                    System.out.println("Exception" + e.getMessage());
                }

            }
        }

        // Write the new chat to the database
        chatRepo.insert(chatMessage);

    }
}