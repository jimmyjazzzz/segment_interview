package erogner.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import erogner.model.ChatMessage;

public interface ChatHistoryRepository extends MongoRepository<ChatMessage, String> {

    List<ChatMessage> getChatMessageByFromUserAndToUser(String fromUser, String toUser);

}