package erogner.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import erogner.model.Account;

public interface AccountRepository extends MongoRepository<Account, String> {

    Account deleteAccountsByName(String name);
    Account getAccountsByName(String name);

}